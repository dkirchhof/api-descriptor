var api = `
users:
  GET /users:
    description: Retrieves all users.
    request:
      headers: []
      paramTypes:
        Query Params:
          params:
          - param: q
            description: filter (prop=value)
            datatype: string
          - param: lang
            description: language
            datatype: string
          example: q=firstname%3Ddaniel&lang=de
    response:
      headers: []
      statusCodes:
        '200':
          params:
          - param: q
            description: filter (prop=value)
            datatype: string
          - param: lang
            description: language
            datatype: string
          example: test

  GET /tools:
    description: Retrieves all tools.
    request:
      headers: []
      paramTypes:
    response:
      headers: []
      statusCodes:
        200:
          params:
          - param: q
            description: filter (prop=value)
            datatype: string
          - param: lang
            description: language
            datatype: string
          example: test
`;
