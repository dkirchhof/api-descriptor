$.views.converters("firstMatch", function(val)
{
  return val.match(/(.*) (.*)/)[1];
});

$.views.converters("secondMatch", function(val)
{
  return val.match(/(.*) (.*)/)[2];
});

$.views.converters("test", function(val)
{
  console.log(val);

  return val;
})

var categoryContainer = $("#categories");
var template = $.templates("#categoriesTemplate");

var api = jsyaml.load(api);

categoryContainer.append(template.render(api));

function collapseExpand(event)
{
  var body = event.parentElement.children[1];
  var display = body.style.display;

  body.style.display = display == "none" ? "block" : "none";
}

var elem;
